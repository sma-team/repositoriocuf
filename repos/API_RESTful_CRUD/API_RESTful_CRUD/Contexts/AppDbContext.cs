﻿using API_RESTful_CRUD.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace API_RESTful_CRUD.Contexts
{
    public class AppDbContext: Microsoft.EntityFrameworkCore.DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options)
        {
        }
        public Microsoft.EntityFrameworkCore.DbSet<TipoDireccion> TipoDireccion {get;set;}
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_RESTful_CRUD.Entities
{
    public class TipoDireccion
    {
        [Key]
        public int TipoDireccionId { get; set; }

        public string Nombre { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_RESTful_CRUD.Contexts;
using API_RESTful_CRUD.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_RESTful_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDireccionController : Controller
    {
        private readonly AppDbContext context;

        public TipoDireccionController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/TipoDireccionController
        [HttpGet]
        public IEnumerable<TipoDireccion> Get()
        {
            return context.TipoDireccion.ToList();
        }

        // GET api/<TipoDireccionController>/5
        [HttpGet("{id}")]
        public TipoDireccion Get(int id)
        {

            var tipoDireccion = context.TipoDireccion.FirstOrDefault(p=>p.TipoDireccionId==id);
            return tipoDireccion;
        }

        // POST api/<TipoDireccionController>
        [HttpPost]
        public ActionResult Post([FromBody] TipoDireccion tipoDireccion)
        {
            try
            {
                context.TipoDireccion.Add(tipoDireccion);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();                 
            }
        }

        // PUT api/<TipoDireccionController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TipoDireccion tipoDireccion)
        {
            if (tipoDireccion.TipoDireccionId==id)
            {
                context.Entry(tipoDireccion).State = EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();     
            }
        }

        // DELETE api/<TipoDireccionController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var tipoDireccion = context.TipoDireccion.FirstOrDefault(p => p.TipoDireccionId == id);
            if (tipoDireccion != null)
            {
                context.TipoDireccion.Remove(tipoDireccion);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
